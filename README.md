# ChatX

ChatX is a chat library for ruby. Jared has volunteered to write a better README. It's all yours Jared. (See [Jareds comment](https://chat.stackoverflow.com/transcript/message/38506296#38506296))

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'chatx'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install chatx

## Usage

Jared will right the rest of this, but here's an example to tide you over:

```ruby
require "chatx"

bot = ChatBot.new("mybot@skynet.com", "skyn3t")

room_num = 1

bot.join_room(room_num)

bot.gen_hooks do
  room room_num do
    on "mention" do |e|
      e.reply "Right!" if e.body.downcase.end_with?("right?")
    end

    on "mention" do |e|
      e.reply "This is my reply!" if e.body.downcase.include?("reply")
    end

    on "message" do |e|
      if e.body.downcase.start_with?("@smelly good")
        tod = e.body.split(" ")[2].gsub(/[^a-zA-Z]/, "")
        e.reply "Good #{tod} to you as well!"
      end
    end

    on "mention" do |e|
      if e.body.downcase.include?("suspense")
        fork do
          sleep 10
          e.reply "HAHA! You thought I died"
        end
      end
    end

    on "message" do |e|
      say "Yup, up and running well" if e.body == "!!/alive"
    end

    on "message" do |e|
      bot.die if e.body.downcase == "@smelly die"
    end

    on "star" do |e|
      puts "Starred! #{e}"
    end
  end
end

bot.die

bot.leave_room(room_num)
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/chatx. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Chatx project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/chatx/blob/master/CODE_OF_CONDUCT.md).
