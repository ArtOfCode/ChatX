require "mechanize"
require "nokogiri"
require "faye/websocket"
require "json"
require "websocket/extensions"
require "permessage_deflate"
require_relative "chatx/events"
require_relative "chatx/auth"
require_relative "chatx/websocket"
require_relative "chatx/hooks"

class ChatBot
  # @!attribute [r] rooms
  #   @return [Hash<Hash<Array>>] a hash of the rooms. Each key is a room ID, and
  #     each value is futher hash with an :events key which contains an array
  #     of {Event}s from that room.
  # @!attribute [r] websocket
  #   @return [Hash<room_id, Thread>] a hash of websockets. Each key is a room ID and
  #     each value is a websocket thread. Each websocket gets it's own thead because
  #     EventMachine blocks the main thread when I run it there.
  # @!attribute [rw] default_server
  #   @return [String] The default server to connect to. It's good to
  #     pass this to the default_server keyword argument of the {initialize} method
  #     if you're only sticking to one server. Otherwise, with every {#say}
  #     you'll have to specify the server. This defaults to stackexchange. The options for
  #     this are "meta.stackexchange", "stackexchange", and "stackoverflow". The rest of
  #     the URL is managed internally.
  # @!attribute [rw] hooks
  #   @return [Array] An array of the {Hook}s for the bot.
  attr_reader :rooms, :websocket
  attr_accessor :default_server, :hooks

  # Creates a bot and attempts to log in to all three chat
  # servers: chat.stackexchange.com, chat.meta.stackexchange.com
  # and chat.stackoverflow.com
  #
  # These must be stack exchange openid credentials, and the user
  # must have spoken in a room on that server first.
  #
  # For further details on authentication, see #authenticate
  #
  # @param [String] email The Stack Exchange OpenID email
  # @param [String] password The Stack Exchange OpenID password
  # @return [ChatBot] A new bot instance. Not sure what happens if
  #   you try and run more than one at a time...
  def initialize(email, password, default_server: "stackexchange")
    @email = email
    @password = password
    @agent = Mechanize.new
    @rooms = {} # room_id => {events}
    @default_server = default_server
    @hooks = {}
    @websocket = {}

    authenticate(["stackexchange", "stackoverflow", "meta.stackexchange"])
  end

  # Attempts to join a room, and for every room joined opens a websocket.
  # Websockets seem to be the way to show your presence in a room. It's weird
  # that way.
  #
  # Each websocket is added to the @websockets instance variable which can be
  # read but not written to.
  #
  # @param room_id [#to_i] A valid room ID on the server designated by the
  #   server param.
  # @keyword server [String] A string referring to the relevant server. The
  #   default value is set by the @default_server instance variable.
  # @return [Hash] The hash of currently active websockets.
  def join_room(room_id, server: @default_server)
    puts "Joining #{room_id}"

    fkey = get_fkey(server, "rooms/#{room_id}")

    events_json = @agent.post("https://chat.#{server}.com/chats/#{room_id}/events",
                              fkey: fkey,
                              since: 0,
                              mode: "Messages",
                              msgCount: 100).body

    events = JSON.parse(events_json)["events"]

    ws_auth_data = @agent.post("https://chat.#{server}.com/ws-auth",
                               roomid: room_id,
                               fkey: fkey)

    @rooms[room_id] = {}
    @rooms[room_id][:events] = events.map { |e| Event::Message.new(e, self) }

    ws_uri = JSON.parse(ws_auth_data.body)["url"]
    last_event_time = events.max_by { |event| event["time_stamp"] }["time_stamp"]
    cookies = (@agent.cookies.map { |cookie| "#{cookie.name}=#{cookie.value}" if cookie.domain == "chat.#{server}.com" || cookie.domain == "#{server}.com" } - [nil]).join("; ")
    @websocket[room_id.to_i] = open_ws("#{ws_uri}?l=#{last_event_time}", cookies, server: server)
  end

  # Leaves the room. Not much else to say...
  # @param room_id [#to_i] The ID of the room to leave
  # @keyword server [String] The chat server that room is on.
  # @return A meaningless value
  def leave_room(room_id, server: @default_server)
    fkey = get_fkey("stackexchange", "/rooms/#{room_id}")
    if !@websocket[room_id.to_i].nil?
      @websocket[room_id.to_i].kill
      @websocket.delete(room_id.to_i)
    end
    @agent.post("https://chat.#{server}.com/chats/leave/#{room_id}", fkey: fkey, quiet: "true")
  end

  # Speaks in a room! Not much to say here, but much to say in the room
  # that is passed!
  #
  # If you're trying to reply to a message, please use the {Message#reply}
  # method.
  # @param room_id [#to_i] The ID of the room to be spoken in
  # @param content [String] The text of message to send
  # @keyword server [String]  The server to send the messon on.
  # @return A meaningless value
  def say(room_id, content, server: @default_server)
    fkey = get_fkey(server, "/rooms/#{room_id}")
    begin
      @agent.post("https://chat.#{server}.com/chats/#{room_id}/messages/new", fkey: fkey, text: content)
    rescue Mechanize::ResponseCodeError => e
      puts "Posting message failed. Retrying..."
      sleep 0.3 # A magic number I just chose for no reason
      retry
    end
  end

  def toggle_star(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey)
  end

  def star_count(message_id, server: @default_server)
    page = @agent.get("https://chat.#{serer}.com/transcript/message/#{message_id}")
    page.css("#message-#{message_id} span.stars.vote-count-container.always .times")[0].content
  end

  def star(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey) unless is_starred?(message_id)
  end

  def unstar(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/star", fkey: fkey) if is_starred?(message_id)
  end

  def is_starred?(message_id, server: @default_server)
    page = @agent.get("https://chat.#{server}.com/transcript/message/#{message_id}")
    page.css("#message-#{message_id} span.stars.vote-count-container.always")[0].attribute("class").include?("user-star")
  end

  def cancel_stars(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/unstar", fkey: fkey)
  end

  def delete(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/delete", fkey: fkey)
  end

  def edit(message_id, new_message, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}", fkey: fkey, text: new_message)
  end

  def toggle_pin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def pin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def unpin(message_id, server: @default_server)
    fkey = get_fkey("stackexchange")
    @agent.post("https://chat.#{server}.com/messages/#{message_id}/owner-star", fkey: fkey)
  end

  def is_pinned?(message_id, server: @default_server)
    page = @agent.get("https://chat.#{serer}.com/transcript/message/#{message_id}")
    page.css("#message-#{message_id} span.stars.vote-count-container.always")[0].attribute("class").include?("owner-star")
  end

  # Kills all active websockets for the bot.
  def die
    @websocket.each { |_room, ws| ws.join }
  end

  private

  def get_fkey(server, uri = "")
    @agent.get("https://chat.#{server}.com/#{uri}").search("//input[@name='fkey']").attribute("value")
  end
end
