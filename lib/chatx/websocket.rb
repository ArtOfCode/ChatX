class ChatBot
  private

  # Opens a websocket. This was really annoying. To open a websocket, you've got
  # to grab a weird uri (see {#join_room}) and pass some cookies and set the origin
  # header. Anywho, it opens a new socket in a new thread.
  # @note This method is internal. Do not use.
  # @param uri [String] See {#join_room}
  # @param cookies [String] See {#join_room}
  # @param server [String] The chat server to use.
  def open_ws(uri, cookies, server: @default_server)
    headers = {
      "Cookie" => cookies,
      "Origin" => "https://chat.#{server}.com"
    }
    puts "Opening websocket"
    Thread.new do
      EM.run do
        ws = Faye::WebSocket::Client.new(uri, nil, headers: headers, extensions: [PermessageDeflate])

        ws.on :open do |_event|
          say(63296, "Hello World")
          p [:open]
        end

        ws.on :message do |event|
          handle event
        end

        ws.on :close do |event|
          say(63296, "I'm shutting down! Sorry!")
          p [:close, event.code, event.reason]
          ws = nil
        end
      end
    end
  end
end
