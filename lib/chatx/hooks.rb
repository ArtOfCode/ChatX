class ChatBot
  # The immediate exit point when a message is recieved from a websocket. It
  # grabs the relevant hooks, creates the event, and passes the event to the
  # hooks.
  #
  # It also spawns a new thread for every hook. This could lead to errors later,
  # but it prevents 409 errors which shut the bot up for a while.
  #
  # @note This method is strictly internal.
  # @param thing [String] It's a string passed by the websocket
  def handle(thing)
    data = JSON.parse(thing.data)
    data.each do |room, event|
      next if event.keys.first != "e"
      event["e"].each do |e|
        event_type = e["event_type"].to_i - 1
        room_id = room[1..-1].to_i
        event = Event.new e, self
        @rooms[room_id][:events].push(event)

        unless @hooks[event_type].nil?
          @hooks[event_type].each do |rm_id, hook|
            fork do
              hook.call(event.event || event.hash) if rm_id == room_id
            end
          end
        end

        content = e["content"]
        puts "#{event_type} in room #{room_id}: #{e}"
      end
    end
  end

  # A convinant way to hook into an event.
  # @param room_id [#to_i] The ID of th room to listen in.
  # @param event [String] The [EVENT_SHORTHAND] for the event ID.
  # @param action [Proc] This is a block which will run when the hook
  #   is triggered. It is passed one parameter, which is the event.
  #   It is important to note that it will NOT be passed an {Event},
  #   rather, it will be passed a sub event designated in {Event::EVENT_CLASSES}.
  def add_hook(room_id, event, &action)
    @hooks[EVENT_SHORTHAND.index(event)] = [] if @hooks[EVENT_SHORTHAND.index(event)].nil?
    @hooks[EVENT_SHORTHAND.index(event)].push [room_id, action]
  end

  # A simpler syntax for creating {add_hook} to the "Message Posted" event.
  # @param room_id [#to_i] The room to listen in
  # @see #add_hook
  def on_message(room_id, &action)
    add_hook(room_id, "Message Posted") { |e| yield(e.hash["content"]) }
  end

  # This opens up the DSL created by {Hook}.
  def gen_hooks(&block)
    hook = Hook.new(self)
    hook.instance_eval(&block)
  end
end

class Hook
  attr_reader :bot

  def initialize(bot)
    @bot = bot
  end

  def say(*args)
    @bot.say(*args)
  end

  def room(room_id, &block)
    room = Room.new(room_id, self)
    room.instance_eval(&block)
  end

  def on(event, room_id: @room_id, bot: @bot, &block)
    @bot.hooks[EVENT_SHORTHAND.index(event)] = [] if bot.hooks[EVENT_SHORTHAND.index(event)].nil?
    @bot.hooks[EVENT_SHORTHAND.index(event)].push [room_id, block]
  end

  class Room < Hook
    def initialize(room_id, hook)
      @hook = hook
      @bot = hook.bot
      @room_id = room_id
    end

    def say(*args)
      @bot.say(@room_id, *args)
    end

    def on(event, &block)
      @hook.on(event, room_id: @room_id, &block)
    end
  end
end
