EVENT = [
  "Message Posted",
  "Message Edited",
  "User Entered",
  "User Left",
  "Room Name Changed",
  "Message Starred",
  "Debug Message",
  "User Mentioned",
  "Message Flagged",
  "Message Deleted",
  "File Added",
  "Moderator Flag",
  "User Settings Changed",
  "Global Notification",
  "Access Level Changed",
  "User Notification",
  "Invitation",
  "Message Reply",
  "Message Moved Out",
  "Message Moved In",
  "Time Break",
  "Feed Ticker",
  "User Suspended",
  "User Merged"
].freeze

EVENT_SHORTHAND = [
  "message",
  "edit",
  "entrance",
  "exit",
  "rename",
  "star",
  "debug",
  "mention",
  "flag",
  "delete",
  "file",
  "mod-flag",
  "settings",
  "gnotif",
  "level",
  "lnotif",
  "invite",
  "reply",
  "move-out",
  "move-in",
  "time",
  "feed",
  "suspended",
  "merged"
].freeze
class Event
  class Message
    attr_reader :timestamp, :body, :room, :user, :id
    attr_reader :room, :room_name

    def initialize(event_hash, bot)
      @timestamp = event_hash["time_stamp"]
      @body = event_hash["content"]
      @room = event_hash["room_id"].to_i
      @user = event_hash["user_id"].to_i
      @id = event_hash["message_id"].to_i
      @bot = bot

      @room = event_hash["room_id"]
      @room_name = event_hash["room_name"]
    end

    def reply(msg)
      @bot.say(@room, ":#{@id} #{msg}")
    end

    # TODO: All of these needs the server

    %i{
      toggle_star
      star_count
      star
      unstar
      is_starred?
      cancel_stars
      delete
      edit
      toggle_pin
      pin
      unpin
      is_pinned?
    }.each do |name|
      define_method(name) { @bot.send(name, @id) }
    end
  end

  class User
    attr_reader :timestamp, :id, :target, :name
    attr_reader :room, :room_name

    def initialize(event_hash, bot)
      @timestamp = event_hash["time_stamp"]
      @id = event_hash["user_id"]
      @target = event_hash["target_user_id"]
      @name = event_hash["user_name"]
      @bot = bot

      @room = event_hash["room_id"]
      @room_name = event_hash["room_name"]
    end
  end
end

class Event
  # TODO: Sometimes, events have both a user and a message, or something like that.
  # how can we cope?
  EVENT_CLASSES = {
    1 => Message,
    2 => Message,
    3 => User,
    4 => User,
    6 => Message,
    8 => Message
  }.freeze

  attr_reader :type, :type_long, :type_short, :hash, :event

  def initialize(event_hash, bot)
    @type = event_hash["event_type"].to_i
    @type_short = EVENT_SHORTHAND[@type]
    @type_long = EVENT_SHORTHAND[@type]
    @bot = bot

    @hash = event_hash # .delete("event_type")

    @event = EVENT_CLASSES[@type].new(@hash, @bot) if EVENT_CLASSES.keys.include? @type
  end
end
