class ChatBot
  def authenticate(sites = ["stackexchange"])
    sites = [sites] unless sites.is_a?(Array)

    openid = @agent.get "https://openid.stackexchange.com/account/login"
    fkey_input = openid.search "//input[@name='fkey']"
    fkey = fkey_input.attribute("value")

    @agent.post("https://openid.stackexchange.com/account/login/submit",
                fkey:     fkey,
                email:    @email,
                password: @password)

    # TODO: Check for incorrect creds

    sites.each do |site|
      site_auth(site)
    end
  end

  def site_auth(site)
    puts "Attempting to login to #{site}"

    # Get fkey
    login_page = @agent.get "https://#{site}.com/users/login"
    fkey_input = login_page.search "//input[@name='fkey']"
    fkey = fkey_input.attribute("value")

    @agent.post("https://#{site}.com/users/authenticate",
                fkey: fkey,
                openid_identifier: "https://openid.stackexchange.com")

    home = @agent.get "https://chat.#{site}.com"
    if home.search(".topbar-links span.topbar-menu-links a").first.text.casecmp "log in"
      puts "Auth failed"
    else
      puts "Authenticated Sucessfully!"
    end
  end
end
